export const SEND_REQUEST = 'SEND_REQUEST';
export const startAction = type => `START_${type}`;
export const successAction = type => `SUCCESS_${type}`;
